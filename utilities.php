<?php
function returnTheSmallestNumber(array $numberElts)
{
    
    /* Nombre d'entrées dans le tableau */
    $entriesNumber;
    /* Index courant de lecture du tableau */
    $i;
    /* Entrée courante du tableau */
    $currentEntry;
    /* Valeur du plus petit nombre */
    $theSmallestNumber;
    
    $entriesNumber = count($numberElts);
    
    for ($i = 0; $i < $entriesNumber; $i++) {
        
        $currentEntry = $numberElts[$i];
        
        /**
        * Condition d'initialisation ($i = 0) 
        * OU de réaffectation ($currentEntry < $theSmallestNumber)
        * de $theSmallestNumber
        */
        if ($i == 0 || $currentEntry < $theSmallestNumber) {
            
            $theSmallestNumber = $currentEntry;
            
        }
        
    }
    
    return $theSmallestNumber;
    
}