<?php
class ArrayManager
{

    private $table;
    
    public function __construct(array $array = []) {
        
        $this->table = $array;
        
    }
    
    /* Getter */
    public function getArray() {
        return $this->table;
    }
    
    /* Setter */
    public function setArray(array $array) {
        $this->table = $array;
    }
    
    public function returnTheSmallestNumber() {
        
        /* Nombre d'entrées dans le tableau */
        $entriesNumber;
        /* Index courant de lecture du tableau */
        $i;
        /* Entrée courante du tableau */
        $currentEntry;
        /* Valeur du plus petit nombre */
        $theSmallestNumber;

        $entriesNumber = count($this->table);

        for ($i = 0; $i < $entriesNumber; $i++) {

            $currentEntry = $this->table[$i];

            /**
            * Condition d'initialisation ($i = 0) 
            * ou de réaffectation ($currentEntry < $theSmallestNumber)
            * de $theSmallestNumber
            */
            if ($i == 0 || $currentEntry < $theSmallestNumber) {

                $theSmallestNumber = $currentEntry;

            }

        }

        return $theSmallestNumber;
        
    }

}